#- vim: softtabstop=4 shiftwidth=4 expandtab

from cStringIO import StringIO
from textwrap import dedent
import os
import nose.tools

import convert_trackinfo as cti

"""Unit tests for convert_trackinfo. To be run with nose ("nosetests" at the
command line).

(get it from <http://somethingaboutorange.com/mrl/projects/nose/>)
"""

def test_convert_time():
    assert cti.convert_time('0.0.0') == 0
    assert cti.convert_time('0.0.01') == 10
    assert cti.convert_time('0.0.10') == 100
    assert cti.convert_time('0.0.1') == 100
    assert cti.convert_time('0.24.26') == 24260
    assert cti.convert_time('0.24.62') == 24620
    assert cti.convert_time('0.54.19') == 54190
    assert cti.convert_time('0.55.00') == 55000


def test_read_empty():
    fileobject = StringIO('')
    assert cti.read_trackinfo(fileobject) == []

def test_read_single():
    fileobject = StringIO('track = BL1') 
    assert cti.read_trackinfo(fileobject) == [('track', 'BL1', [])]

def test_read_flat():
    fileobject = StringIO(dedent("""\
        Split1Action = TRI_split1_0:TRI_split1_1
        Split2Action = TRI_split2_0:TRI_split2_1
        Split3Action = TRI_split3_0:TRI_split3_1
        LapTimeAction = TRI_lap_0:TRI_lap_1
        """))
    assert cti.read_trackinfo(fileobject) == [
        ('Split1Action', 'TRI_split1_0:TRI_split1_1', []),
        ('Split2Action', 'TRI_split2_0:TRI_split2_1', []),
        ('Split3Action', 'TRI_split3_0:TRI_split3_1', []),
        ('LapTimeAction', 'TRI_lap_0:TRI_lap_1', [])
        ]

def test_read_nested_simple():
    fileobject = StringIO(dedent("""\
        track = BL1
        \tlen = 3.4
        \tcar = XFR
        """))
    nose.tools.eq_(
        cti.read_trackinfo(fileobject),
        [('track', 'BL1', [('len', '3.4', []), ('car', 'XFR', [])])]
    )

def test_read_nested_extended():
    fileobject = StringIO(dedent("""\
        track = BL1
        \tlen = 3.4
        \tcar = XFR
        \t\tsplit1 = 0.24.26:0.24.62
        \t\tsplit2 = 0.54.19:0.55.00
        track = AS4
        \tlen = 5.3
        \tcar = FOX
        """))
    assert cti.read_trackinfo(fileobject) == [
        ('track', 'BL1', [
            ('len', '3.4', []),
            ('car', 'XFR', [
                ('split1', '0.24.26:0.24.62', []),
                ('split2', '0.54.19:0.55.00', [])
                ])
            ]),
        ('track', 'AS4', [
            ('len', '5.3', []),
            ('car', 'FOX', [])
            ])
        ]

def test_read_wrong_indent():
    fileobject = StringIO(dedent("""\
        track = BL1
        \t\tsplit1 = 0.24.26:0.24.62
        \ttrack = AS4
        """))
    nose.tools.assert_raises(SyntaxError, cti.read_trackinfo, fileobject)

def test_read_from_real_file():
    testfilename = 'test.cfg'
    testfile = open(testfilename, 'wt')
    testfile.write('track = BL1\n')
    testfile.close()
    assert cti.read_trackinfo(testfilename) == [('track', 'BL1', [])]
    os.remove(testfilename)

def test_read_comment():
    fileobject = StringIO(dedent("""\
        track = BL1
        # this is a comment line
        \tlen = 3.4
        # this is a comment line
        \t# this is a comment line
        \tcar = XFR
        """))
    assert cti.read_trackinfo(fileobject) == [
        ('track', 'BL1', [
            ('len', '3.4', []),
            ('car', 'XFR', [])
            ])
        ]

def test_read_blank_lines():
    fileobject = StringIO(dedent("""\
        track = BL1
        
        \tlen = 3.4
        
        \t
        \tcar = XFR
        """))
    assert cti.read_trackinfo(fileobject) == [
        ('track', 'BL1', [
            ('len', '3.4', []),
            ('car', 'XFR', [])
            ])
        ]


def test_get_splits_empty():
    assert cti.get_splits([]) == []

def test_get_splits_simple():
    trackinfo = [
        ('track', 'BL1',[
            ('car', 'XFR', [
                ('split1', '0.24.26:0.24.62', []),
                ('split2', '0.54.19:0.55.00', []),
                ('lapTime', '1.15.48:1.16.61', [])
                ])
            ]
        )]
    assert cti.get_splits(trackinfo) == [
        ('BL1', [
            ('XFR', [
                (24620, 24260), 
                (55000, 54190), 
                (76610, 75480)
                ])
            ])
        ]

def test_get_track_splits_empty():
    assert cti.get_track_splits([]) == []

def test_get_track_splits_single():
    children = [
        ('car', 'XFR', [
            ('split1', '0.24.26:0.24.62', []),
            ('split2', '0.54.19:0.55.00', []),
            ('lapTime', '1.15.48:1.16.61', [])
            ])
        ]
    assert cti.get_track_splits(children) == [
            ('XFR', [
                (24620, 24260), 
                (55000, 54190), 
                (76610, 75480)
                ])
            ]

def test_get_car_splits_empty():
    assert cti.get_car_splits([]) == []

def test_get_car_splits_single():
    children = [
        ('split1', '0.24.26:0.24.62', []),
        ('split2', '0.54.19:0.55.00', []),
        ('lapTime', '1.15.48:1.16.61', [])
        ]
    assert cti.get_car_splits(children) == [
        (24620, 24260), 
        (55000, 54190), 
        (76610, 75480)
        ]

def test_write_splits_empty():
    fileobject = StringIO()
    cti.write_splits([], fileobject)
    assert fileobject.getvalue() == dedent("""\
        Splits = {
        };
        """)

def test_write_splits():
    splits = [
        ('BL1', [
            ('XFR', [
                (24620, 24260), 
                (55000, 54190), 
                (76610, 75480)
                ])
            ])
        ]
    fileobject = StringIO()
    cti.write_splits(splits, fileobject)
    assert fileobject.getvalue() == dedent("""\
        Splits = {
            BL1 = {
                XFR = { good1 = 24620, great1 = 24260, good2 = 55000, great2 = 54190, good3 = 76610, great3 = 75480 },
            },
        };
        """)

def test_write_real_file():
    testfilename = 'test.lua'
    splits = [
        ('BL1', [
            ('XFR', [
                (24620, 24260), 
                (55000, 54190), 
                (76610, 75480)
                ])
            ])
        ]
    cti.write_splits(splits, testfilename)
    testfile = open(testfilename, 'rt')
    assert testfile.read() == dedent("""\
        Splits = {
            BL1 = {
                XFR = { good1 = 24620, great1 = 24260, good2 = 55000, great2 = 54190, good3 = 76610, great3 = 75480 },
            },
        };
        """)
    testfile.close()
    os.remove(testfilename)
