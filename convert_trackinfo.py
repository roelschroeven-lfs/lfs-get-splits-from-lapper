#- vim: softtabstop=4 shiftwidth=4 expandtab

import sys
from textwrap import dedent

"""Read splits from LFS lapper trackinfo files and write them to LUA format."""

def convert_time(lapper_time):
    """Convert time from lapper notation (e.g. 0.24.26) to Dirk's notation
    (in thousands of a second, e.g. 24640)."""
    min, sec, hund = lapper_time.split('.')
    # if hund is too short, pad right with a zero
    if len(hund) == 1:
        hund = hund + '0'
    min, sec, hund = map(int, (min, sec, hund))
    return (min * 60 + sec) * 1000 + hund * 10

def read_trackinfo(file):
    """Read an LFS lapper trackinfo file.
    The file parameter is either a filename or a file-like object.
    The result is a list of tuples: each input line (except comment lines and
    blank lines) becomes a tuple: name, value, list of children.
    """
    def count_leading_tabs(s):
        i = 0
        while s[i] == '\t':
            i += 1
        return i
    # If file is a filename, open the file
    if isinstance(file, basestring):
        fileobject = open(file, 'rt')
    else:
        fileobject = file
    # Keep a stack of indent levels and corresponding lists
    level_stack = [([], 0)]
    for linenr, line in enumerate(fileobject):
        line = line.rstrip()
        if line.lstrip().startswith('#'): continue  # Ignore comment lines
        if not line: continue # Ignore empty lines
        # Find indent level, go up or down the stack as needed
        level = count_leading_tabs(line)
        current_list, current_level = level_stack[-1]
        if level > current_level:
            level_stack.append((current_list[-1][2], level))
            current_list, current_level = level_stack[-1]
        else:
            while level < current_level:
                level_stack.pop()
                current_list, current_level = level_stack[-1]
        if level != current_level:
            raise SyntaxError, 'wrong indent level on line %d' % (linenr + 1)
        # Parse the line into name and value, and append it to the list
        name, sep, value = line.partition('=')
        current_list.append((name.strip(), value.strip(), []))
    # Close the file in case we opened it ourselves (otherwise it's up to the
    # caller)
    if isinstance(file, basestring):
        fileobject.close()
    return level_stack[0][0]

def get_splits(trackinfo):
    """Read the splits from trackinfo and return a list of tuples like this:

        ('BL1', [
            ('XFR', [
                (24620, 24260), 
                (55000, 54190), 
                (76610, 75480)
                ])
            ])
    """
    splits = []
    for name, value, children in trackinfo:
        if name.lower() == 'track':
            splits.append((value, get_track_splits(children)))
    return splits

def get_track_splits(track_children):
    track_splits = []
    for name, value, children in track_children:
        if name.lower() == 'car':
            track_splits.append((value, get_car_splits(children)))
    return track_splits

def get_car_splits(car_children):
    splits = []
    for name, value, _ in car_children:
        if name.lower().startswith('split') or name.lower() == 'laptime':
            great, good = value.split(':')
            great = convert_time(great)
            good = convert_time(good)
            splits.append((good, great))
    return splits

def write_splits(splits, file):
    """Write the splits to file in Lua-format.
    Specify either a filename or a file-like object."""
    # If file is a filename, open the file
    if isinstance(file, basestring):
        fileobject = open(file, 'wt')
    else:
        fileobject = file
    # Write the splits
    fileobject.write('Splits = {\n')
    for track, cars in splits:
        fileobject.write('    %s = {\n' % track);
        for car, splits in cars:
            fileobject.write('        %s = { ' % car)
            for i, (good, great) in enumerate(splits):
                nr = i + 1
                if i > 0:
                    fileobject.write(', ')
                fileobject.write('good%d = %d, great%d = %d' % (
                    nr, good, nr, great))
            fileobject.write(' },\n')
        fileobject.write('    },\n');
    fileobject.write('};\n')
    # Close the file in case we opened it ourselves (otherwise it's up to the
    # caller)
    if isinstance(file, basestring):
        fileobject.close()


def print_usage():
    print dedent("""\
        convert_trackinfo.py: read splits from LFS lapper trackinfo

        Usage:

            convert_trackinfo.py <input_file> [<output_file>]

            If called without output_file, the output will be written to
            stdout.
        """)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print_usage()
    else:
        infile = sys.argv[1]
        if len(sys.argv) == 2:
            outfile = sys.stdout
        else:
            outfile = sys.argv[2]
        trackinfo = read_trackinfo(infile)
        splits = get_splits(trackinfo)
        write_splits(splits, outfile)

